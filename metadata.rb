name             'icinga-client'
maintainer       'Alexander Skwar'
maintainer_email 'a@skwar.me'
license          'All rights reserved'
description      'Setup node as Icinga Client, My Style'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '2.0.0'
