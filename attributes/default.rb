
#default["icinga-client"][:icinga][:config_dir] = '/etc/nagios'
#default["icinga-client"][:icinga][:nrpe_plugin_dir] = '/usr/lib/nagios/plugins/'
default["icinga-client"][:nrpe_service] = 'nagios-nrpe-server'

# Apps
default["icinga-client"][:apache][:http_port] = 80
default["icinga-client"][:timeserver] = 'ch.pool.ntp.org'

# Pakete
#default["icinga-client"][:packages] = %w(libnagios-plugin-perl libwww-perl nagios-nrpe-server nagios-plugins nagios-plugins-basic)

# Zu installierende Pakete
default["icinga-client"][:pakete] = %w[nagios-plugins nagios-plugins-basic nagios-plugins-standard nagios-nrpe-server libnagios-plugin-perl libwww-perl]

# Konfigurationsparameter für nrpe
default["icinga-client"][:nrpe][:cfg][:log_facility] = 'daemon'
default["icinga-client"][:nrpe][:cfg][:server_port] = 5666
default["icinga-client"][:nrpe][:cfg][:nrpe_user] = 'daemon'
default["icinga-client"][:nrpe][:cfg][:nrpe_group] = 'daemon'
default["icinga-client"][:nrpe][:cfg][:allowed_hosts] = '192.168.177.11,192.168.177.12,212.71.125.220,10.8.3.210'
default["icinga-client"][:nrpe][:cfg][:dont_blame_nrpe] = 1
default["icinga-client"][:nrpe][:cfg][:debug] = 0
default["icinga-client"][:nrpe][:cfg][:command_timeout] = 60
default["icinga-client"][:nrpe][:cfg][:connection_timeout] = 300

default["icinga-client"][:nrpe][:cfg][:pid_file] = case node[:platform]
    when 'ubuntu'
        case node[:lsb][:codename]
            when 'xenial' then '/run/nagios/nrpe.pid'
            else '/var/run/nagios/nrpe.pid'
        end # of case node[:lsb][:codename]
    else '/var/run/nrpe/nrpe.pid'
end # of default["icinga-client"][:nrpe][:cfg][:pid_file] = case node[:platform]

# Pfad des Konfigurationsverzeichnisses
default["icinga-client"][:cfg_path] = case node[:os]
    when 'linux' then '/etc/nagios'
    when 'freebsd' then '/usr/local/etc/nrpe2'
    when 'solaris' then '/etc/opt/webstack'
    when 'smartos' then '/opt/local/etc/nagios'
end # of default["icinga-client"][:cfg_path] = case node[:os]

# Pfad des Nagios Plugins Verzeichnisses
default["icinga-client"][:plugins_path] = case node[:platform_family]
    when 'debian' then '/usr/lib/nagios/plugins'
    when 'redhat' then '/usr/lib64/nagios/plugins'
    when 'freebsd' then '/usr/local/lib/nagios/plugins'
end

# EOF
