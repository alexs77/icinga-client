#
# Cookbook Name:: icinga-client
# Recipe:: default
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

# Default Werte bestimmen
#include_recipe "#{cookbook_name}::default_values"

# Pakete installieren
include_recipe "#{cookbook_name}::install_packages"

# Verzeichnisse anlegen
include_recipe "#{cookbook_name}::directories"

# Checks kopieren
include_recipe "#{cookbook_name}::checks"

# Konfigurationsdateien kopieren
include_recipe "#{cookbook_name}::configuration"

# EOF
