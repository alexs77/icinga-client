#
# Cookbook Name:: icinga-client
# Recipe:: configuration
#
# Copyrigt 2017, Alexander Skwar
#
# All rights reserved - Do Not Redistribute
#

###################### DEFAULT VALUES
# Lese Werte aus Data Bag und falls nicht vorhanden, node Attribute nehmen
nrpe_service = node[cookbook_name][:nrpe_service]
nrpe_plugin_dir = node[cookbook_name][:plugins_path]
icinga_config_dir = node[cookbook_name][:cfg_path]
timeserver = node[cookbook_name][:timeserver]
apache_port = node[cookbook_name][:apache][:http_port]
packages = node[cookbook_name][:pakete]
cfg = node[cookbook_name][:nrpe][:cfg]

if search(cookbook_name, "id:#{node['hostname']}").any?
    dbi = data_bag_item(cookbook_name, node['hostname'])

    nrpe_service = dbi['nrpe_service'] if dbi.has_key? 'nrpe_service'
    nrpe_plugin_dir = dbi['icinga']['nrpe_plugin_dir'] if dbi.has_key? 'icinga'
    icinga_config_dir = dbi['icinga']['config_dir'] if dbi.has_key? 'icinga'
    timeserver = dbi['timeservers'].first if dbi.has_key? 'timeservers'
    apache_port = dbi['apache']['http_port'] if dbi.has_key? 'apache'
    packages = dbi['packages'] if dbi.has_key? 'packages'
    cfg = dbi['cfg'] if dbi.has_key? 'cfg'
end # of if search(cookbook_name, "id:#{node['hostname']}").any?
###################### DEFAULT VALUES

# Konfigurationsdateien kopieren
remote_directory "Install nrpe configuration - files" do
    files_owner 'root'
    files_group node[:root_group]
    files_mode 0o444
    owner 'root'
    group node[:root_group]
    mode 0o755
    path icinga_config_dir
    source 'sysconfigdir'
    notifies :restart, "service[#{nrpe_service}]", :delayed
end # of remote_directory "Install nrpe configuration - files" do

# Basis Konfiguration
template 'nrpe Basis Konfiguration: ' + ::File.join(icinga_config_dir, "nrpe.cfg") do
    source ::File.join('sysconfigdir', 'nrpe.cfg.erb')
    path ::File.join(icinga_config_dir, "nrpe.cfg")
    owner 'root'
    group node[:root_group]
    mode 0o644
    action :create

    variables({
        :cfg => cfg,
        :cfg_path => icinga_config_dir
    })

    notifies :restart, 'service[nagios-nrpe-server]', :delayed
end # of template 'nrpe Basis Konfiguration: nrpe.cfg' do
