# icinga-client Cookbook

Richte Icinga Client auf Node nach My Style ein.

## Data Bag

Es wird optional das Data Bag `icinga-client` verwendet. Definition:

```json
{
  "id": "beispiel-mit-allen-unterstützten-attributen",
  "nrpe_service": "nagios-nrpe-server",
  "icinga": {
    "nrpe_plugin_dir": "/usr/lib/nagios/plugins",
    "config_dir": "/etc/nagios"
  },
  "timeservers": [
    "pool.ntp.org"
  ],
  "apache": {
    "http_port": 80
  },
  "packages": [
    libnagios-plugin-perl, libwww-perl, nagios-nrpe-server, nagios-plugins
  ]
}
```

## Attributes

```ruby

default['icinga-client']['icinga']['config_dir'] = /etc/nagios
default['icinga-client']['icinga']['nrpe_plugin_dir'] = /usr/lib/nagios/plugins/
default['icinga-client']['nrpe_service'] = 'nrpe-server'

# Apps
default['icinga-client']['apache']['http_port'] = 80
default['icinga-client']['timeserver'] = 'pool.ntp.org'

# Pakete
default['icinga-client']['packages'] = %w(libnagios-plugin-perl libwww-perl nagios-nrpe-server nagios-plugins nagios-plugins-basic)
```

TODO: List your cookbook attributes here.

## Usage

### icinga-client::default

e.g.
Just include `icinga-client` in your node's `run_list`:

```json
{
  "name": "my_node",

  "run_list": [
    "recipe[icinga-client]"
  ]
}
```

## License and Authors

Authors: Alexander Skwar

---

[//]: Links
